const zodiacSigns = [
	"",//add an empty element so that the succeding month's index numbers will coincide with actual month numbers, ie. January==1
	{
		month: "January",//zodiacSigns[1].month
		border: 20,
		signs: [//nested array
			{//zodiacSigns[1].signs[0].name
				name: "Aquarius",
				//zodiacSigns[1].signs[0].horoscope()
				horoscope: function(){//method named horoscope
					console.log(this.name);
					return "Deep and creative " + this.name;
				}
			},
			{
				name: "Capricorn",
				horoscope: function(){
					console.log(this.name);
					return "Serious and Strong " + this.name;
				}
			},
		]
	},
	{
		month: "February",
		border: 19,
		signs: [
			{
				name: "Pisces",
				horoscope: function(){
					console.log(this.name);
					return "Wise and artistic " + this.name;
				}
			},
			{
				name: "Aquarius",
				horoscope: function(){
					console.log(this.name);
					return "Deep and creative " + this.name;
				}
			},
		]
	},
	{
		month: "March",
		border: 21,
		signs: [
			{
				name: "Aries",
				horoscope: function(){
					console.log(this.name);
					return "Eager and quick " + this.name;
				}
			},
			{
				name: "Pisces",
				horoscope: function(){
					console.log(this.name);
					return "Wise and artistic " + this.name;
				}
			},
		]
	},
	{
		month: "April",
		border: 20,
		signs: [
			{
				name: "Taurus",
				horoscope: function(){
					console.log(this.name);
					return "Strong and reliable " + this.name;
				}
			},
			{
				name: "Aries",
				horoscope: function(){
					console.log(this.name);
					return "Eager and quick " + this.name;
				}
			},
		]
	},
	{
		month: "May",
		border: 21,
		signs: [
			{
				name: "Gemini",
				horoscope: function(){
					console.log(this.name);
					return "Curious and kind " + this.name;
				}
			},
			{
				name: "Taurus",
				horoscope: function(){
					console.log(this.name);
					return "Strong and reliable " + this.name;
				}
			},
		]
	},
	{
		month: "June",
		border: 21,
		signs: [
			{
				name: "Cancer",
				horoscope: function(){
					console.log(this.name);
					return "Sensitive and loyal " + this.name;
				}
			},
			{
				name: "Gemini",
				horoscope: function(){
					console.log(this.name);
					return "Curious and kind " + this.name;
				}
			},
		]
	},
	{
		month: "July",
		border: 23,
		signs: [
			{
				name: "Leo",
				horoscope: function(){
					console.log(this.name);
					return "Fiery and confident " + this.name;
				}
			},
			{
				name: "Cancer",
				horoscope: function(){
					console.log(this.name);
					return "Sensitive and loyal " + this.name;
				}
			},
		]
	},
	{
		month: "August",
		border: 23,
		signs: [
			{
				name: "Virgo",
				horoscope: function(){
					console.log(this.name);
					return "Gentle and smart " + this.name;
				}
			},
			{
				name: "Leo",
				horoscope: function(){
					console.log(this.name);
					return "Fiery and confident " + this.name;
				}
			},
		]
	},
	{
		month: "September",
		border: 23,
		signs: [
			{
				name: "Libra",
				horoscope: function(){
					console.log(this.name);
					return "Sociable and fair " + this.name;
				}
			},
			{
				name: "Virgo",
				horoscope: function(){
					console.log(this.name);
					return "Gentle and smart " + this.name;
				}
			},
		]
	},
	{
		month: "October",
		border: 23,
		signs: [
			{
				name: "Scorpio",
				horoscope: function(){
					console.log(this.name);
					return "Original and brave " + this.name;
				}
			},
			{
				name: "Libra",
				horoscope: function(){
					console.log(this.name);
					return "Sociable and fair " + this.name;
				}
			},
		]
	},
	{
		month: "November",
		border: 22,
		signs: [
			{
				name: "Saggitarius",
				horoscope: function(){
					console.log(this.name);
					return "Funny and generous " + this.name;
				}
			},
			{
				name: "Scorpio",
				horoscope: function(){
					console.log(this.name);
					return "Original and brave " + this.name;
				}
			},
		]
	},
	{
		month: "December",
		border: 22,
		signs: [
			{
				name: "Capricorn",
				horoscope: function(){
					console.log(this.name);
					return "Serious and Strong " + this.name;
				}
			},
			{
				name: "Saggitarius",
				horoscope: function(){
					console.log(this.name);
					return "Funny and generous " + this.name;
				}
			},
		]
	}
];

//create a function zodiacChecker
const zodiacChecker = function(month, day){
	if(typeof month === "number" &&
		typeof day === "number"){
		if((month > 0 && month <= 12) && (day > 0 && day <= 31)){
			//determine the zodiac sign based on a given month and day
			if(day >= zodiacSigns[month].border){
        const thePrediction = zodiacSigns[month].signs[0].horoscope();
        document.querySelector('#prediction').innerHTML = `Here is your Horoscope: ${thePrediction}`;
			}else{
        const thePrediction = zodiacSigns[month].signs[1].horoscope();
        document.querySelector('#prediction').innerHTML = `Here is your Horoscope: ${thePrediction}`;
			}
		}else{
      document.querySelector('#prediction').innerHTML = "To reveal your horoscope, the Month must be from \"1 to 12\" and day must be from \"1 to 31\".";
			console.log("Month must be from 0 to 12 and day must be from 0 to 31.")
		}
	}else{
		console.log("Both month and day input values must be of data type number.");
	}
}

//convert the parameters to DOM parameters
let month = document.querySelector('#monthNumber');
let day = document.querySelector('#dayNumber');
const predictButton = document.querySelector('form button');

//attach an event listener to our predictButton
predictButton.addEventListener("click", function(){
		const monthValue = Number(month.value);
    const dayValue = Number(day.value);
    zodiacChecker(monthValue, dayValue);
		//console.log(`Your birth month: ${monthValue}`);
    //console.log(`Your birth day: ${dayValue}`);
});
